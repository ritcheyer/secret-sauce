class AddPaymentTypeRefToOrders < ActiveRecord::Migration
  def change
    add_reference :orders, :payment_type, index: true

    # migrate existing data
    execute "update orders set payment_type_id = 1 where pay_type = 'Check'"
    execute "update orders set payment_type_id = 2 where pay_type = 'Credit Card'"
    execute "update orders set payment_type_id = 3 where pay_type = 'Purchase Order'"

    remove_column :orders, :pay_type

  end
end
