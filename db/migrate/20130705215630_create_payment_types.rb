class CreatePaymentTypes < ActiveRecord::Migration
  def up
    create_table :payment_types do |t|
      t.column :name, :string, :null => false
    end

    PaymentType.create(:name => 'Check')
    PaymentType.create(:name => 'Credit Card')
    PaymentType.create(:name => 'Purchase Order')

    # disallow null and set a default value for payment_type_id
    # change_column :orders, :payment_type_id, :integer, :null => false, :default => 1
  end

  def down
    add_column :orders, :pay_type, :string, :limit => 10

    execute "alter table orders drop foreign key fk_order_payment_types" 
    execute "update orders set pay_type = 'Check' where payment_type_id = 1"
    execute "update orders set pay_type = 'Credit Card' where payment_type_id = 2"
    execute "update orders set pay_type = 'Purchase Order where payment_type_id = 3"

    remove_column :orders, :payment_type_id

    drop_table :payment_types
  end
end
