# Reference Link: http://media.pragprog.com/titles/rails4/code/rails40/depot_g/db/migrate/20121130000004_add_quantity_to_line_items.rb

class AddQuantityToLineItems < ActiveRecord::Migration
  def change
    add_column :line_items, :quantity, :integer, default: 1
  end
end
