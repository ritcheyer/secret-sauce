class PaymentTypeNameMigration < ActiveRecord::Migration
  def up

    add_column :orders, :payment_type_id, :integer
    rename_column :payment_types, :label, :name

    Order.all.each do |order|

      order.payment_type = PaymentType.where(:name => order.pay_type.downcase).first_or_create
      order.save!
      
      puts "#{order.inspect}"

    end
    
    puts "ready baby all day long"

    remove_column :orders, :pay_type
    remove_column :payment_types, :value

  end

  def down
    add_column :orders, :pay_type, :string

    Order.all.each do |order|

      order.pay_type = order.payment_type.name
      order.save

    end

    rename_column :payment_types, :name, :label

    remove_column :orders, :payment_type_id

    add_column :payment_types, :value
  end
end
