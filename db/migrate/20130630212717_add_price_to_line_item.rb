# Reference Link: http://intertwingly.net/projects/AWDwR4/checkdepot-40/section-10.4.html

class AddPriceToLineItem < ActiveRecord::Migration
  def change
    add_column :line_items, :price, :decimal
    LineItem.all.each do |li|
      li.price = li.product.price
    end
  end
end
