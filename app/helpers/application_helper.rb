# Reference Link: http://media.pragprog.com/titles/rails4/code/rails40/depot_n/app/helpers/application_helper.rb

module ApplicationHelper
  def hidden_div_if(condition, attributes = {}, &block)
    if condition
      attributes["style"] = "display: none"
    end
    content_tag("div", attributes, &block)
  end
end
