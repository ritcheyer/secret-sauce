class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_action :checkout_enabled, :is_editable

  def checkout_enabled
    @is_checkout_page = true
  end
  
  def is_editable
    @editable = true
  end
end
