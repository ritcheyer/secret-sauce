# Reference Link: http://media.pragprog.com/titles/rails4/code/rails40/depot_k/app/controllers/store_controller.rb

class StoreController < ApplicationController
  include CurrentCart
  before_action :set_cart

  def index
    @products = Product.order(:title)
    @count = increment_count
    @time = Time.now
  end

  def increment_count
    if session[:counter].nil?
      session[:counter] = 0
    end
    session[:counter] += 1
    if session[:counter] > 5
      return session[:counter]
    end
  end

end
