# Reference Link: http://media.pragprog.com/titles/rails4/code/rails40/depot_o/app/models/line_item.rb
class LineItem < ActiveRecord::Base
  belongs_to :order
  belongs_to :product
  belongs_to :cart
  
  def total_price
    product.price * quantity
  end

end
