# Reference Link: http://media.pragprog.com/titles/rails4/code/rails40/depot_r/test/integration/user_stories_test.rb

require 'test_helper'

class UserStoriesTest < ActionDispatch::IntegrationTest
  fixtures :products

  # A user goes to the index page. They select a product, adding it to their
  # cart, and check out, filling in their details on the checkout form. When
  # they submit, an order is created containing their information, along with a
  # single line item corresponding to the product they added to their cart.

  test "buying a product" do

    # STEP 0: Clean it up first
    LineItem.delete_all
    Order.delete_all
    ruby_book = products(:ruby)

    # STEP 1: A user goes to the store index page
    get "/"
    assert_response :success
    assert_template "index"

    # STEP 2: They select a product
    xml_http_request :post, '/line_items', product_id: ruby_book.id
    assert_response :success

    # STEP 3: They add it to their cart
    cart = Cart.find(session[:cart_id])
    assert_equal 1, cart.line_items.size
    assert_equal ruby_book, cart.line_items[0].product

    # STEP 4: They then check out
    get "/orders/new"
    assert_response :success
    assert_template "new"

    # STEP 5: User enters their information during checkout
    post_via_redirect "/orders",
                      order: {
                        name:     "Dave Thomas",
                        address:  "123 The Street",
                        email:    "dave@example.com",
                        pay_type: "Check"
                      }
    assert_response :success
    assert_template "index"
    cart = Cart.find(session[:cart_id])
    assert_equal 0, cart.line_items.size
    
    # STEP 6a: Verify there is only 1 record in the database
    orders = Order.all
    assert_equal 1, orders.size
    order = orders[0]
    
    # STEP 6b: Verify the data matches what we sent to it.
    assert_equal "Dave Thomas",      order.name
    assert_equal "123 The Street",   order.address
    assert_equal "dave@example.com", order.email
    assert_equal "Check",            order.payment_type.name
    
    # STEP 7: Verify mail is correctly address and has expected subject line
    mail = ActionMailer::Base.deliveries.last
    assert_equal ["dave@example.com"], mail.to
    assert_equal 'Sam Ruby <depot@example.com>', mail[:from].value
    assert_equal "Pragmatic Store Order Confirmation", mail.subject
  end
  
  
  
end
